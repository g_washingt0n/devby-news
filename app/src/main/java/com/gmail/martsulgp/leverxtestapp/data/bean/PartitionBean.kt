package com.gmail.martsulgp.leverxtestapp.data.bean

data class PartitionBean(
    val partitionTitle: String,
    val articles: List<ArticleBean>
)