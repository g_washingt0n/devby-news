package com.gmail.martsulgp.leverxtestapp.data.mapper

import com.gmail.martsulgp.leverxtestapp.data.ResourceService
import com.gmail.martsulgp.leverxtestapp.data.bean.ArticleBean
import com.gmail.martsulgp.leverxtestapp.data.bean.PartitionBean
import com.gmail.martsulgp.leverxtestapp.data.bean.WebsiteBean
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements


object WebsitePageMapper {

    private const val urlBase = ResourceService.baseUrlAddress

    fun mapToWebsite(document: Document): WebsiteBean {

        val siteLogoElement = document.selectFirst(".navbar__brand")
        val actualPartitionTitle =
            document.select(".island__header").elementAt(2).takePartitionHeader()
        val newsPartitionTitle =
            document.select(".island__header").elementAt(0).takePartitionHeader()
        val actualPartition: Element = document.selectFirst(".cards-group_items-4")
        val newsPartition: Element = document.select(".island__body-main").elementAt(2)

        val siteLogoUrl = urlBase + parseLogo(siteLogoElement)

        return WebsiteBean(
            websiteLogoUrl = siteLogoUrl,
            newsPartitions = listOfNotNull(
                getActualPartitionBean(
                    actualPartitionTitle,
                    actualPartition
                ),
                getMewsPartitionBean(
                    newsPartitionTitle,
                    newsPartition
                )
            )
        )
    }

    private fun parseLogo(element: Element): String {
        var address = ""
        element.apply {
            select("navbar__brand-img")
            childNodes().forEach {
                if (!it.attr("src").isNullOrEmpty()) {
                    address = it.attr("src")
                }
            }
        }
        return address
    }

    private fun getActualPartitionBean(actualTitle: String, element: Element): PartitionBean {
        val actualArticlesList = element.select(".card").parseAsArticlesList()

        return PartitionBean(
            partitionTitle = actualTitle,
            articles = actualArticlesList
        )
    }

    private fun getMewsPartitionBean(newsTitle: String, element: Element): PartitionBean {
        val newsArticlesList = element.select(".card.card_media").parseAsArticlesList()

        return PartitionBean(
            partitionTitle = newsTitle,
            articles = newsArticlesList
        )
    }

    private fun Element.takePartitionHeader() = this.select("h2").text()

    private fun Elements.parseAsArticlesList(): List<ArticleBean> {
        val articlesList: MutableList<ArticleBean> = arrayListOf()
        forEach {
            var pic = urlBase
            var url = urlBase
            var title: String

            with(it.select(".card__img-wrap").select(".card__img")) {
                pic += this.attr("src")
                title = this.attr("title")
            }
            url += it.select(".card__info").select(".card__body")
                .select("a")
                .attr("href")
            articlesList.add(
                ArticleBean(
                    articleUrl = url,
                    articlePicUrl = pic,
                    articleTitle = title
                )
            )
        }
        return articlesList
    }
}