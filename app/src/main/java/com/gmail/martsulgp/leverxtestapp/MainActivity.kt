package com.gmail.martsulgp.leverxtestapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gmail.martsulgp.leverxtestapp.ui.main.FeedFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, FeedFragment.newInstance())
                .commitNow()
        }
    }
}
