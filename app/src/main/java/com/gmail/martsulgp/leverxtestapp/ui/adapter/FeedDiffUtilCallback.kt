package com.gmail.martsulgp.leverxtestapp.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.gmail.martsulgp.leverxtestapp.ui.listItem.BaseItem


class FeedDiffUtilCallback(
    private val oldList: List<BaseItem>,
    private val newList: List<BaseItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = oldList[oldItemPosition]
        return oldItem.getObjectComparator(newItem)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = oldList[oldItemPosition]
        return oldItem.getContentComparator(newItem)
    }
}