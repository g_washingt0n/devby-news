package com.gmail.martsulgp.leverxtestapp.data

import io.reactivex.Single
import org.jsoup.nodes.Document

interface NewsRepository {

    fun getNewsFeed(): Single<Document>
//    fun updateNewsFeed(): Single<WebsitePage>
}

class NewsRepositoryImpl(
    private val service: ResourceService
) : NewsRepository {

    override fun getNewsFeed(): Single<Document> {
        return service.getNews()
    }

//    override fun updateNewsFeed() {
//        return service.getNews()
//    }
}