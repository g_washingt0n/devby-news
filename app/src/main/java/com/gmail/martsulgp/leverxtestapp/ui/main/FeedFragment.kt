package com.gmail.martsulgp.leverxtestapp.ui.main

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.gmail.martsulgp.leverxtestapp.R
import com.gmail.martsulgp.leverxtestapp.ui.adapter.BaseAdapter
import com.gmail.martsulgp.leverxtestapp.ui.details.DetailsFragment
import com.gmail.martsulgp.leverxtestapp.ui.listItem.ArticleListItem
import com.gmail.martsulgp.leverxtestapp.ui.listItem.BaseItem
import com.gmail.martsulgp.leverxtestapp.ui.listItem.PartitionListItem
import org.koin.androidx.viewmodel.ext.android.viewModel

class FeedFragment : Fragment(), BaseAdapter.ItemController {

    private lateinit var headerImage: ImageView
    private lateinit var recycler: RecyclerView
    private lateinit var progressView: ProgressBar

    companion object {
        fun newInstance() = FeedFragment()
    }

    private val viewModel: FeedViewModel by viewModel()

    private val adapter = BaseAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.feed_fragment, container, false)
        headerImage = view.findViewById(R.id.feed_topImage)
        progressView = view.findViewById(R.id.feed_loader)
        recycler = view.findViewById(R.id.feed_recyclerView)
        recycler.apply {
            adapter = this@FeedFragment.adapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
    }

    //Temporary decision, better create a unique base functionality
    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    override fun itemClicked(url: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, DetailsFragment.newInstance(url))
            .addToBackStack(null)
            .commit()
    }

    private fun initViewModel() {
        viewModel.headerLogoLiveData.observe(this, Observer { url ->
            loadImage(url, headerImage)
        })
        viewModel.newsFeedLiveData.observe(this, Observer { beansList ->
            if (!beansList.isNullOrEmpty()) {
                val adapterItemsList: MutableList<BaseItem> = arrayListOf()
                beansList.map { bean ->
                    adapterItemsList.add(
                        PartitionListItem(
                            bean.partitionTitle
                        )
                    )
                    bean.articles.forEach {
                        adapterItemsList.add(
                            ArticleListItem(
                                articlePicUrl = it.articlePicUrl,
                                articleUrl = it.articleUrl,
                                articleTitle = it.articleTitle
                            )
                        )
                    }
                }

                adapter.setController(this)
                adapter.dataChanged(adapterItemsList)
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            Toast.makeText(this.context, "Failed to receive data", Toast.LENGTH_LONG).show()

        })
        viewModel.loadingLiveData.observe(this, Observer { loading ->
            progressView.visibility = if (loading) View.VISIBLE else View.GONE
        })
    }

    private fun loadImage(url: String, targetView: ImageView) {
        GlideToVectorYou.justLoadImage(activity, Uri.parse(url), targetView)
    }

}
