package com.gmail.martsulgp.leverxtestapp.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class NewsPartition(
    @PrimaryKey val partitionId: Int,
    @ColumnInfo(name = "websiteForPartitionId") val websiteForPartitionId: Long?,
    @ColumnInfo(name = "partition_title") val partitionTitle: String?
)