package com.gmail.martsulgp.leverxtestapp.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.gmail.martsulgp.leverxtestapp.R

private const val URL_KEY = "url_key"

class DetailsFragment : Fragment() {

    lateinit var webView: WebView


    companion object {
        fun newInstance(url: String): DetailsFragment {
            val bundle = Bundle().apply {
                this.putString(URL_KEY, url)
            }
            val fragment = DetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.details_fragment, container, false)
        webView = view.findViewById(R.id.details_webView)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        webView.loadUrl(arguments?.getString(URL_KEY))
    }
}