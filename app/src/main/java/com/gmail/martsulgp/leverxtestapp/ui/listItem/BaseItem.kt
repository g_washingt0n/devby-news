package com.gmail.martsulgp.leverxtestapp.ui.listItem

import android.view.View
import com.gmail.martsulgp.leverxtestapp.ui.adapter.BaseAdapter

interface BaseItem {

    fun getView(): Int

    fun renderView(view: View, positionInAdapter: Int = 0, controller: BaseAdapter.ItemController)

    fun getObjectComparator(newItem: BaseItem): Boolean

    fun getContentComparator(newItem: BaseItem): Boolean

}