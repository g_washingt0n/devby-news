package com.gmail.martsulgp.leverxtestapp.data.bean

data class ArticleBean(
    val articlePicUrl: String,
    val articleUrl: String,
    val articleTitle: String
)