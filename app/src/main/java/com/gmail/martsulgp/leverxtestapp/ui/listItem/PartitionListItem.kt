package com.gmail.martsulgp.leverxtestapp.ui.listItem

import android.view.View
import android.widget.TextView
import com.gmail.martsulgp.leverxtestapp.R
import com.gmail.martsulgp.leverxtestapp.ui.adapter.BaseAdapter

class PartitionListItem(
    var title: String
) : BaseItem {

    private lateinit var partitionTitle: TextView

    override fun getView() = R.layout.item_partition

    override fun renderView(
        view: View,
        positionInAdapter: Int,
        controller: BaseAdapter.ItemController
    ) {
        partitionTitle = view.findViewById(R.id.partition_title)
        partitionTitle.text = title
    }

    override fun getObjectComparator(newItem: BaseItem): Boolean {
        return if (newItem is PartitionListItem) {
            this.title == newItem.title
        } else {
            this === newItem
        }
    }

    override fun getContentComparator(newItem: BaseItem): Boolean {
        return if (newItem is PartitionListItem) {
            this.title == newItem.title
        } else {
            this == newItem
        }
    }
}
