package com.gmail.martsulgp.leverxtestapp.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Article(
    @PrimaryKey val articleId: Int,
    @ColumnInfo(name = "partitionForArticleId") val partitionForArticleId: Long?,
    @ColumnInfo(name = "article_pic") val articlePicUrl: String?,
    @ColumnInfo(name = "article_url") val articleUrl: String?,
    @ColumnInfo(name = "article_title") val articleTitle: String?
)