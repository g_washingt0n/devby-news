package com.gmail.martsulgp.leverxtestapp.data.dao

import androidx.room.*
import com.gmail.martsulgp.leverxtestapp.data.entity.WebsiteAndPartitions
import com.gmail.martsulgp.leverxtestapp.data.entity.WebsitePage

@Dao
interface WebsiteDao {

    @Transaction
    @Query("SELECT * FROM WebsitePage")
    fun getPage(): WebsiteAndPartitions

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWebsitePage(page: WebsitePage)

    @Transaction
    @Update
    fun updatePage(page: WebsitePage)
}