package com.gmail.martsulgp.leverxtestapp.di.modules

import androidx.room.Room
import com.gmail.martsulgp.leverxtestapp.data.NewsRepository
import com.gmail.martsulgp.leverxtestapp.data.NewsRepositoryImpl
import com.gmail.martsulgp.leverxtestapp.data.ResourceService
import com.gmail.martsulgp.leverxtestapp.data.WebsiteDataBase
import com.gmail.martsulgp.leverxtestapp.domain.GetNewsUseCaseImpl
import com.gmail.martsulgp.leverxtestapp.ui.main.FeedViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val DEV_BY_DB_NAME = "dev_by_db_name"
private const val IO_SCHEDULER = "io"
private const val MAIN_SCHEDULER = "scheduler"


val feedModule = module {


    viewModel<FeedViewModel> {
        FeedViewModel(
            getNewsUseCase = GetNewsUseCaseImpl(
                dataBaseDao = get(),
                newsRepository = get(),
                subscribeScheduler = get(named(IO_SCHEDULER)),
                observeScheduler = get(named(MAIN_SCHEDULER))
            ),
            disposables = get()
        )
    }

    single<CompositeDisposable> { CompositeDisposable() }

    single(named(IO_SCHEDULER)) { Schedulers.io() }

    single(named(MAIN_SCHEDULER)) { AndroidSchedulers.mainThread() }

    single<NewsRepository> { NewsRepositoryImpl(get()) }

    single { ResourceService.create() }

    single {
        Room.databaseBuilder(
            androidContext(),
            WebsiteDataBase::class.java, DEV_BY_DB_NAME
        ).build().websiteDao()
    }
}