package com.gmail.martsulgp.leverxtestapp.ui.listItem

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.gmail.martsulgp.leverxtestapp.R
import com.gmail.martsulgp.leverxtestapp.ui.adapter.BaseAdapter
import com.squareup.picasso.Picasso

class ArticleListItem(
    val articlePicUrl: String,
    val articleUrl: String,
    val articleTitle: String
) : BaseItem, View.OnClickListener {

    private lateinit var articleCard: CardView
    private lateinit var articleImage: ImageView
    private lateinit var articleTextTitle: TextView

    private var localController: BaseAdapter.ItemController? = null

    override fun getView() = R.layout.item_article

    override fun renderView(
        view: View,
        positionInAdapter: Int,
        controller: BaseAdapter.ItemController
    ) {
        view.setOnClickListener(this)
        localController = controller
        articleCard = view.findViewById(R.id.article_card)
        articleImage = view.findViewById(R.id.article_logo)
        loadImage(articlePicUrl, articleImage)
        articleTextTitle = view.findViewById(R.id.article_title)
        articleTextTitle.text = articleTitle
    }

    override fun getObjectComparator(newItem: BaseItem): Boolean {
        return if (newItem is ArticleListItem) {
            this.articleUrl == newItem.articleUrl
        } else {
            this === newItem
        }
    }

    override fun getContentComparator(newItem: BaseItem): Boolean {
        return if (newItem is ArticleListItem) {
            this.articleUrl == newItem.articleUrl &&
                    this.articlePicUrl == newItem.articlePicUrl &&
                    this.articleTitle == newItem.articleTitle
        } else {
            this == newItem
        }
    }

    override fun onClick(v: View?) {
        when (v!!) {
            articleCard -> localController?.itemClicked(articleUrl)
        }
    }

    private fun loadImage(url: String, targetView: ImageView) {
        Picasso.get()
            .load(url)
            .into(targetView)
    }
}
