package com.gmail.martsulgp.leverxtestapp.di

import android.app.Application
import com.gmail.martsulgp.leverxtestapp.di.modules.feedModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(
                feedModule
            )
        }
    }
}