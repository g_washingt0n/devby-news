package com.gmail.martsulgp.leverxtestapp.data.bean

data class WebsiteBean(
    val websiteLogoUrl: String,
    val newsPartitions: List<PartitionBean>
)