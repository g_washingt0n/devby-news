package com.gmail.martsulgp.leverxtestapp.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.gmail.martsulgp.leverxtestapp.ui.listItem.BaseItem


class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindItem(holderItem: BaseItem, controller: BaseAdapter.ItemController) {
        holderItem.renderView(itemView, adapterPosition, controller)
    }
}