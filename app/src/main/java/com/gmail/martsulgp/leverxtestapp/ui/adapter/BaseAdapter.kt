package com.gmail.martsulgp.leverxtestapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gmail.martsulgp.leverxtestapp.ui.listItem.BaseItem

class BaseAdapter : RecyclerView.Adapter<BaseHolder>() {

    private var items: MutableList<BaseItem> = arrayListOf()

    private lateinit var controller: ItemController

    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        holder.bindItem(items[position], controller)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int = items[position].getView()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
        return BaseHolder(root)
    }


    fun dataChanged(items: List<BaseItem>) {
        val diffUtillCallback =
            FeedDiffUtilCallback(this.items, items)
        val result = DiffUtil.calculateDiff(diffUtillCallback, true)
        this.items.clear()
        this.items.addAll(items)
        result.dispatchUpdatesTo(this)
    }

    fun setController(controller: ItemController) {
        this.controller = controller
    }


    interface ItemController {
        fun itemClicked(url: String)
    }

}