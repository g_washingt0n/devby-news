package com.gmail.martsulgp.leverxtestapp.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WebsitePage(
    @PrimaryKey val websiteId: Int,
    @ColumnInfo(name = "website_logo") val websiteLogoUrl: String?
)