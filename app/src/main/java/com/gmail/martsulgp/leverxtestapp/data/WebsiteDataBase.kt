package com.gmail.martsulgp.leverxtestapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gmail.martsulgp.leverxtestapp.data.dao.WebsiteDao
import com.gmail.martsulgp.leverxtestapp.data.entity.Article
import com.gmail.martsulgp.leverxtestapp.data.entity.NewsPartition
import com.gmail.martsulgp.leverxtestapp.data.entity.WebsitePage

@Database(entities = [WebsitePage::class, NewsPartition::class, Article::class], version = 1)
abstract class WebsiteDataBase : RoomDatabase() {
    abstract fun websiteDao(): WebsiteDao
}