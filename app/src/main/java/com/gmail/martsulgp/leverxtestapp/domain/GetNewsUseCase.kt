package com.gmail.martsulgp.leverxtestapp.domain

import com.gmail.martsulgp.leverxtestapp.data.NewsRepository
import com.gmail.martsulgp.leverxtestapp.data.bean.WebsiteBean
import com.gmail.martsulgp.leverxtestapp.data.dao.WebsiteDao
import com.gmail.martsulgp.leverxtestapp.data.mapper.WebsitePageMapper
import io.reactivex.Scheduler
import io.reactivex.Single

interface GetNewsUseCase {
    //    fun execute(): Flowable<WebsiteAndPartitions>
    fun execute(): Single<WebsiteBean>
}

class GetNewsUseCaseImpl(
    private val dataBaseDao: WebsiteDao,
    private val newsRepository: NewsRepository,
    private val subscribeScheduler: Scheduler,
    private val observeScheduler: Scheduler
) : GetNewsUseCase {
    override fun execute(): Single<WebsiteBean> {
        return newsRepository.getNewsFeed()
            .map {
                WebsitePageMapper.mapToWebsite(document = it)
            }
            .subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
    }

//TODO edit realization with DB interaction

//    override fun execute(): Flowable<WebsiteAndPartitions> {
//        return dataBaseDao.getPage()                          // Error interacting DB from main thread
//            .toSingle()
//            .concatWith {
//                newsRepository.getNewsFeed()
//                    .map {
//                        WebsitePageMapper.mapToWebsite(document = it)
//                    }
//                    .doOnSuccess {
//                        dataBaseDao.insertWebsitePage(it)
//                    }
//            }
//            .subscribeOn(subscribeScheduler)
//            .observeOn(observeScheduler)
//    }
}