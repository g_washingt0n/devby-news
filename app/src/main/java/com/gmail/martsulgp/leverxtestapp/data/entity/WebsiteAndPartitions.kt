package com.gmail.martsulgp.leverxtestapp.data.entity

import androidx.room.Embedded
import androidx.room.Relation

data class WebsiteAndPartitions(
    @Embedded val website: WebsitePage,
    @Relation(
        parentColumn = "websiteId",
        entityColumn = "websiteForPartitionId"
    )
    val partitions: List<NewsPartition>
)