package com.gmail.martsulgp.leverxtestapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gmail.martsulgp.leverxtestapp.data.bean.PartitionBean
import com.gmail.martsulgp.leverxtestapp.domain.GetNewsUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class FeedViewModel(
    getNewsUseCase: GetNewsUseCase,
    private val disposables: CompositeDisposable
) : ViewModel() {

    private val _headerLogoLiveData = MutableLiveData<String>()
    private val _newsFeedLiveData = MutableLiveData<List<PartitionBean>>()
    private val _errorLiveData = MutableLiveData<Throwable>()
    private val _loadingLiveData = MutableLiveData<Boolean>()

    val headerLogoLiveData: LiveData<String> = _headerLogoLiveData
    val newsFeedLiveData: LiveData<List<PartitionBean>> = _newsFeedLiveData
    val errorLiveData: LiveData<Throwable> = _errorLiveData
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    init {
        getNewsUseCase.execute()
            .doOnSubscribe { _loadingLiveData.value = true }
            .doFinally { _loadingLiveData.value = false }
            .subscribeBy(
                onSuccess = {
                    _headerLogoLiveData.value = it.websiteLogoUrl
                    _newsFeedLiveData.value = it.newsPartitions
                },
                onError = {
                    _errorLiveData.value = it
                }
            ).addTo(disposables)
    }

    fun onStop() {
        disposables.clear()
    }
}
