package com.gmail.martsulgp.leverxtestapp.data.entity

import androidx.room.Embedded
import androidx.room.Relation

data class PartitionAndArticle(
    @Embedded val partition: NewsPartition,
    @Relation(
        parentColumn = "partitionId",
        entityColumn = "partitionForArticleId"
    )
    val articles: List<Article>
)