package com.gmail.martsulgp.leverxtestapp.data

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.GET
import java.io.IOException
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit


interface ResourceService {

    @GET(".")
    fun getNews(): Single<Document>

    companion object {
        const val baseUrlAddress = "https://dev.by"

        fun create(): ResourceService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(PageAdapter.FACTORY)
                .baseUrl(baseUrlAddress)
                .client(
                    OkHttpClient.Builder()
                        .readTimeout(20, TimeUnit.SECONDS)
                        .connectTimeout(20, TimeUnit.SECONDS)
                        .addInterceptor(
                            HttpLoggingInterceptor()
                                .setLevel(HttpLoggingInterceptor.Level.BODY)
                        )
                        .build()
                )
                .build()
            return retrofit.create(ResourceService::class.java)

        }
    }
}

internal class PageAdapter : Converter<ResponseBody, Document> {

    @Throws(IOException::class)
    override fun convert(responseBody: ResponseBody): Document {
        return Jsoup.parse(responseBody.string())
    }

    companion object {
        val FACTORY: Converter.Factory = object : Converter.Factory() {

            override fun responseBodyConverter(
                type: Type,
                annotations: Array<Annotation?>?,
                retrofit: Retrofit?
            ): Converter<ResponseBody, *>? {
                return if (type === Document::class.java) PageAdapter() else null
            }
        }
    }
}
